﻿namespace RegistryHopRestService.Infrastructure.Application.Infrastructure.Attributes
{
    using System.Web.Mvc;

    public class EnableCORS : ActionFilterAttribute
    {
        const string AllowControlAllowOriginHeader = "Access-Control-Allow-Origin";
        const string AccessControlAllowHeadersHeader = "Access-Control-Allow-Headers";
        const string AccessControlAllowMethodsHeader = "Access-Control-Allow-Methods";

        public string[] Domains
        {
            get;
            set;
        } = new string[] { "*" };

        public string[] Headers
        {
            get;
            set;
        } = new string[] { "Origin, X-Requested-With, Content-Type, Accept" };

        public string[] Methods
        {
            get;
            set;
        } = new string[] { "GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS" };

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            filterContext.RequestContext.HttpContext.Response.AddHeader(AllowControlAllowOriginHeader, string.Join(",", Domains));
            filterContext.RequestContext.HttpContext.Response.AddHeader(AccessControlAllowHeadersHeader, string.Join(",", Headers));
            filterContext.RequestContext.HttpContext.Response.AddHeader(AccessControlAllowMethodsHeader, string.Join(",", Methods));

            base.OnActionExecuting(filterContext);
        }
    }
}