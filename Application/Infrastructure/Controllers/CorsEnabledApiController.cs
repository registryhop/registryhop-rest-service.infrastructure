﻿namespace RegistryHopRestService.Infrastructure.Application.Infrastructure.Controllers
{
    using System.Net;
    using System.Web.Mvc;
    using RegistryHopRestService.Infrastructure.Application.Infrastructure.Attributes;

    [EnableCORS]
    public abstract class CorsEnabledApiController : SimpleApiController
    {
        public const string ForbiddenResourceErrorMessage = "You do not have access to this resource.";

        [HttpGet]
        public virtual ActionResult Index()
        {
            return HttpResponse(HttpStatusCode.Forbidden, new {
                Message = ForbiddenResourceErrorMessage
            });
        }

        [HttpOptions]
        [ActionName("Index")]
        public void Options()
        {
            Response.StatusCode = (int)HttpStatusCode.OK;
        }

    }
}

