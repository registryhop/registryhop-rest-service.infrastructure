﻿namespace RegistryHopRestService.Infrastructure.Application.Infrastructure.Controllers
{
    using System;
    using System.Net;
    using System.Web.Mvc;
    using Newtonsoft.Json;
    using RegistryHopRestService.Infrastructure.Application.Infrastructure.Attributes;

    [EnableCORS]
    public abstract class SimpleApiController : Controller
    {
        const string JsonContentType = "application/json";

        public ActionResult HttpResponse(HttpStatusCode status, Object body)
        {
            Response.StatusCode = (int)status;
            return Content(JsonConvert.SerializeObject(body), JsonContentType);
        }

        public ActionResult HttpError(HttpStatusCode status, String errorMessage)
        {
            Response.StatusCode = (int)status;
            return Content(JsonConvert.SerializeObject(new {
                error = errorMessage
            }), JsonContentType);
        }
    }
}
